#include <coroutine>
#include <cstddef>

struct meow {
    int i;
    struct promise_type {
        meow get_return_object() { return {}; }
        std::suspend_never initial_suspend() { return {}; }
        std::suspend_never final_suspend() noexcept { return {}; }
        void unhandled_exception() {}
    };

    void* operator new(size_t) { throw; }                   /* <operator-name> ::= nw        # new                                      */
    void* operator new[](size_t) { throw; }                 /*                 ::= na        # new[]                                    */
    void operator delete(void*) { }                         /*                 ::= dl        # delete                                   */
    void operator delete[](void*) { }                       /*                 ::= da        # delete[]                                 */
    std::suspend_never operator co_await() { return {}; }   /*                 ::= aw        # co_await                                 */
    void operator+() {}                                     /*                 ::= ps        # + (unary)                                */
    void operator-() {}                                     /*                 ::= ng        # - (unary)                                */
    void operator&() {}                                     /*                 ::= ad        # & (unary)                                */
    void operator*() {}                                     /*                 ::= de        # * (unary)                                */
    void operator~() {}                                     /*                 ::= co        # ~                                        */
    void operator+(int) {}                                  /*                 ::= pl        # +                                        */
    void operator-(int) {}                                  /*                 ::= mi        # -                                        */
    void operator*(int) {}                                  /*                 ::= ml        # *                                        */
    void operator/(int) {}                                  /*                 ::= dv        # /                                        */
    void operator%(int) {}                                  /*                 ::= rm        # %                                        */
    void operator&(int) {}                                  /*                 ::= an        # &                                        */
    void operator|(int) {}                                  /*                 ::= or        # |                                        */
    void operator^(int) {}                                  /*                 ::= eo        # ^                                        */
    void operator=(int) {}                                  /*                 ::= aS        # =                                        */
    void operator+=(int) {}                                 /*                 ::= pL        # +=                                       */
    void operator-=(int) {}                                 /*                 ::= mI        # -=                                       */
    void operator*=(int) {}                                 /*                 ::= mL        # *=                                       */
    void operator/=(int) {}                                 /*                 ::= dV        # /=                                       */
    void operator%=(int) {}                                 /*                 ::= rM        # %=                                       */
    void operator&=(int) {}                                 /*                 ::= aN        # &=                                       */
    void operator|=(int) {}                                 /*                 ::= oR        # |=                                       */
    void operator^=(int) {}                                 /*                 ::= eO        # ^=                                       */
    void operator<<(int) {}                                 /*                 ::= ls        # <<                                       */
    void operator>>(int) {}                                 /*                 ::= rs        # >>                                       */
    void operator<<=(int) {}                                /*                 ::= lS        # <<=                                      */
    void operator>>=(int) {}                                /*                 ::= rS        # >>=                                      */
    void operator==(int) {}                                 /*                 ::= eq        # ==                                       */
    void operator!=(int) {}                                 /*                 ::= ne        # !=                                       */
    void operator<(int) {}                                  /*                 ::= lt        # <                                        */
    void operator>(int) {}                                  /*                 ::= gt        # >                                        */
    void operator<=(int) {}                                 /*                 ::= le        # <=                                       */
    void operator>=(int) {}                                 /*                 ::= ge        # >=                                       */
    void operator<=>(int) {}                                /*                 ::= ss        # <=>                                      */
    void operator!() {}                                     /*                 ::= nt        # !                                        */
    void operator&&(int) {}                                 /*                 ::= aa        # &&                                       */
    void operator||(int) {}                                 /*                 ::= oo        # ||                                       */
    void operator++(int) {}                                 /*                 ::= pp        # ++ (postfix in <expression> context)     */
    void operator--(int) {}                                 /*                 ::= mm        # -- (postfix in <expression> context)     */
    void operator,(int) {}                                  /*                 ::= cm        # ,                                        */
    void operator->*(int) {}                                /*                 ::= pm        # ->*                                      */
    meow* operator->() { return this; }                     /*                 ::= pt        # ->                                       */
    void operator()() {}                                    /*                 ::= cl        # ()                                       */
    void operator[](int) {}                                 /*                 ::= ix        # []                                       */
    /* ??? */                                               /*                 ::= qu        # ?                                        */
    operator int() { throw; }                               /*                 ::= cv <type> # (cast)                                   */
};
void operator ""_meow(const char *) {}                      /*                 ::= li <source-name>          # operator ""              */
/* ??? */                                                   /*                 ::= v <digit> <source-name>   # vendor extended operator */


int main() {
    meow m;
    meow *mp = new meow;                                    /* <operator-name> ::= nw        # new                                      */
    meow *mp2 = new meow[2];                                /*                 ::= na        # new[]                                    */
    delete mp;                                              /*                 ::= dl        # delete                                   */
    delete[] mp2;                                           /*                 ::= da        # delete[]                                 */
    [] -> meow { co_await meow{}; }();                      /*                 ::= aw        # co_await                                 */
    +m;                                                     /*                 ::= ps        # + (unary)                                */
    -m;                                                     /*                 ::= ng        # - (unary)                                */
    &m;                                                     /*                 ::= ad        # & (unary)                                */
    *m;                                                     /*                 ::= de        # * (unary)                                */
    ~m;                                                     /*                 ::= co        # ~                                        */
    m+1;                                                    /*                 ::= pl        # +                                        */
    m-1;                                                    /*                 ::= mi        # -                                        */
    m*1;                                                    /*                 ::= ml        # *                                        */
    m/1;                                                    /*                 ::= dv        # /                                        */
    m%1;                                                    /*                 ::= rm        # %                                        */
    m&1;                                                    /*                 ::= an        # &                                        */
    m|1;                                                    /*                 ::= or        # |                                        */
    m^1;                                                    /*                 ::= eo        # ^                                        */
    m=1;                                                    /*                 ::= aS        # =                                        */
    m+=1;                                                   /*                 ::= pL        # +=                                       */
    m-=1;                                                   /*                 ::= mI        # -=                                       */
    m*=1;                                                   /*                 ::= mL        # *=                                       */
    m/=1;                                                   /*                 ::= dV        # /=                                       */
    m%=1;                                                   /*                 ::= rM        # %=                                       */
    m&=1;                                                   /*                 ::= aN        # &=                                       */
    m|=1;                                                   /*                 ::= oR        # |=                                       */
    m^=1;                                                   /*                 ::= eO        # ^=                                       */
    m<<1;                                                   /*                 ::= ls        # <<                                       */
    m>>1;                                                   /*                 ::= rs        # >>                                       */
    m<<=1;                                                  /*                 ::= lS        # <<=                                      */
    m>>=1;                                                  /*                 ::= rS        # >>=                                      */
    m==1;                                                   /*                 ::= eq        # ==                                       */
    m!=1;                                                   /*                 ::= ne        # !=                                       */
    m<1;                                                    /*                 ::= lt        # <                                        */
    m>1;                                                    /*                 ::= gt        # >                                        */
    m<=1;                                                   /*                 ::= le        # <=                                       */
    m>=1;                                                   /*                 ::= ge        # >=                                       */
    m<=>1;                                                  /*                 ::= ss        # <=>                                      */
    !m;                                                     /*                 ::= nt        # !                                        */
    m&&1;                                                   /*                 ::= aa        # &&                                       */
    m||1;                                                   /*                 ::= oo        # ||                                       */
    m++;                                                    /*                 ::= pp        # ++ (postfix in <expression> context)     */
    m--;                                                    /*                 ::= mm        # -- (postfix in <expression> context)     */
    m,1;                                                    /*                 ::= cm        # ,                                        */
    m->*1;                                                  /*                 ::= pm        # ->*                                      */
    [[maybe_unused]] int i1 = m->i;                         /*                 ::= pt        # ->                                       */
    m();                                                    /*                 ::= cl        # ()                                       */
    m[1];                                                   /*                 ::= ix        # []                                       */
    /* ??? */                                               /*                 ::= qu        # ?                                        */
    [[maybe_unused]] int i2 = static_cast<int>(m);          /*                 ::= cv <type> # (cast)                                   */
    123_meow;                                               /*                 ::= li <source-name>          # operator ""              */
    /* ??? */                                               /*                 ::= v <digit> <source-name>   # vendor extended operator */
}
